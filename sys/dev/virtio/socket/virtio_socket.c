/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2023, The FreeBSD Foundation
 * Author: Mina Galić <freebsd@igalic.co>
 *
 */

/* Driver for VirtIO socket device. */

#include <sys/types.h>
#include <sys/systm.h>
#include <sys/bus.h>
#include <sys/domain.h>
#include <sys/eventhandler.h>
#include <sys/kernel.h>
#include <sys/lock.h>
#include <sys/malloc.h>
#include <sys/mbuf.h>
#include <sys/module.h>
#include <sys/protosw.h>
#include <sys/sglist.h>
#include <sys/socket.h>
#include <sys/sx.h>
#include <sys/sysctl.h>
#include <sys/uio.h>

#include <machine/resource.h>

#include <dev/virtio/virtio.h>
#include <dev/virtio/virtqueue.h>

#include "virtio_socket.h"

#define VSOCK_DBG_NONE	  0x0
#define VSOCK_DBG_INFO	  0x1
#define VSOCK_DBG_ERR	  0x2
#define VSOCK_DBG_VERBOSE 0x3

#define MAX_PORT	  ((uint32_t)0xFFFFFFFF)
#define MIN_PORT	  ((uint32_t)0x0)

SYSCTL_NODE(_net, OID_AUTO, vsock, CTLFLAG_RD, 0, "VirtIO socket");

static int vs_dbg_level;
SYSCTL_INT(_net_vsock, OID_AUTO, vs_dbg_level, CTLFLAG_RWTUN, &vs_dbg_level,
    VSOCK_DBG_VERBOSE,
    "virtio socket debug level: 0 = none, 1 = info, 2 = error, 3 = verbose");

#define VSOCK_DBG(level, ...)                \
	do {                                 \
		if (vs_dbg_level >= (level)) \
			printf(__VA_ARGS__); \
	} while (0)

MALLOC_DEFINE(M_VSOCK, "virtio_socket", "VirtIO Socket control structures");

/* Globals */

static struct sx vs_trans_socks_sx;
static struct mtx vs_trans_socks_mtx;
static uint32_t prev_autobound_port;

/*
 * LIST globals
 */
static LIST_HEAD(, vs_pcb) vs_trans_bound_socks;
static LIST_HEAD(, vs_pcb) vs_trans_connected_socks;
#define VS_LIST_BOUND	  0x01
#define VS_LIST_CONNECTED 0x02
#define VS_LIST_ALL	  (VS_LIST_BOUND | VS_LIST_CONNECTED)

/*
 * List handling helpers
 * These functions take the form of
 * vslist_<verb>_<object>()
 * for example: vslist_insert_socket()
 *
 * Each function takes a struct mtx *mutex; If mutex is not NULL, locking and
 * unlocking is performed inside the functions.
 */

static struct socket *
vslist_find_socket(struct sockaddr_vm *addr, uint8_t list, struct mtx *mutex)
{
	struct vs_pcb *p = NULL;
	if (mutex != NULL)
		mtx_lock(mutex);

	if (list & VS_LIST_BOUND)
		LIST_FOREACH(p, &vs_trans_bound_socks, bound_next)
			if (p->so != NULL &&
			    addr->svm_port == p->local_addr.svm_port)
				return p->so;

	if (list & VS_LIST_CONNECTED)
		LIST_FOREACH(p, &vs_trans_connected_socks, connected_next)
			if (p->so != NULL &&
			    addr->svm_port == p->local_addr.svm_port)
				return p->so;

	if (mutex != NULL)
		mtx_unlock(mutex);

	return NULL;
}

static void
vslist_insert_socket(struct socket *so, uint8_t list, struct mtx *mutex)
{
	struct vs_pcb *pcb = so->so_pcb;

	if (mutex != NULL)
		mtx_lock(mutex);

	if (list & VS_LIST_BOUND)
		LIST_INSERT_HEAD(&vs_trans_bound_socks, pcb, bound_next);

	if (list & VS_LIST_CONNECTED)
		LIST_INSERT_HEAD(&vs_trans_connected_socks, pcb,
		    connected_next);
}

static void
vslist_remove_pcb(struct vs_pcb *pcb, uint8_t list, struct mtx *mutex)
{
	struct vs_pcb *p = NULL;
	VSOCK_DBG(VSOCK_DBG_VERBOSE, "%s: pcb is %p\n", __func__, pcb);

	if (pcb == NULL)
		return;

	if (mutex != NULL)
		mtx_lock(mutex);

	if (list & VS_LIST_BOUND) {
		LIST_FOREACH(p, &vs_trans_bound_socks, bound_next)
			if (p == pcb)
				LIST_REMOVE(p, bound_next);
	}

	if (list & VS_LIST_CONNECTED) {
		LIST_FOREACH(p, &vs_trans_connected_socks, connected_next)
			if (p == pcb)
				LIST_REMOVE(pcb, connected_next);
	}

	if (mutex != NULL)
		mtx_unlock(mutex);
}

static void
vslist_remove_socket(struct socket *so, uint8_t list, struct mtx *mutex)
{
	struct vs_pcb *pcb = NULL;
	if (!so || !so->so_pcb) {
		VSOCK_DBG(VSOCK_DBG_VERBOSE, "%s: socket or so_pcb is null\n",
		    __func__);
		return;
	}

	pcb = so->so_pcb;
	vslist_remove_pcb(pcb, list, mutex);
}

/* protosw functions */
static int vs_dom_probe(void);
static int vs_trans_attach(struct socket *, int, struct thread *);
static void vs_trans_detach(struct socket *);
static int vs_trans_soreceive(struct socket *, struct sockaddr **, struct uio *,
    struct mbuf **, struct mbuf **, int *);
static int vs_trans_sosend(struct socket *, struct sockaddr *, struct uio *,
    struct mbuf *, struct mbuf *, int, struct thread *);
static void vs_trans_abort(struct socket *);
static void vs_trans_close(struct socket *);
static int vs_trans_bind(struct socket *, struct sockaddr *, struct thread *);
static int vs_trans_listen(struct socket *, int, struct thread *);
static int vs_trans_accept(struct socket *, struct sockaddr *);
static int vs_trans_connect(struct socket *, struct sockaddr *,
    struct thread *);
static int vs_trans_peeraddr(struct socket *, struct sockaddr *);
static int vs_trans_sockaddr(struct socket *, struct sockaddr *);
static int vs_trans_disconnect(struct socket *);
static int vs_trans_shutdown(struct socket *, enum shutdown_how);

/* protosw helper functions */
static int vsock_send_data(struct virtqueue *, struct uio *, uint32_t,
    struct sockbuf *);
static void vs_addr_init(struct sockaddr_vm *, const struct virtio_vsock_hdr *);

/*
 * VirtIO sockets
 */
static struct protosw vt_socket_protosw = {
	.pr_type = SOCK_STREAM,
	.pr_protocol = VIRTIO_SOCK_PROTO_TRANS,
	.pr_flags = PR_CONNREQUIRED,
	.pr_attach = vs_trans_attach,
	.pr_bind = vs_trans_bind,
	.pr_listen = vs_trans_listen,
	.pr_accept = vs_trans_accept,
	.pr_connect = vs_trans_connect,
	.pr_peeraddr = vs_trans_peeraddr,
	.pr_sockaddr = vs_trans_sockaddr,
	.pr_soreceive = vs_trans_soreceive,
	.pr_sosend = vs_trans_sosend,
	.pr_disconnect = vs_trans_disconnect,
	.pr_close = vs_trans_close,
	.pr_detach = vs_trans_detach,
	.pr_shutdown = vs_trans_shutdown,
	.pr_abort = vs_trans_abort,
};

static struct domain vs_socket_domain = {
	.dom_family = AF_VSOCK,
	.dom_name = "virtio",
	.dom_flags = DOMF_UNLOADABLE,
	.dom_probe = vs_dom_probe,
	.dom_nprotosw = 1,
	.dom_protosw = { &vt_socket_protosw },
};

DOMAIN_SET(vs_socket_);

static int
vs_dom_probe(void)
{
	/* currently, we know that KVM and VMWare support VirtIO Socket */
	if (vm_guest != VM_GUEST_VMWARE && vm_guest != VM_GUEST_KVM) {
		return (ENXIO);
	}
	return (0);
}

static inline void
vs_trans_lock(void)
{
	sx_xlock(&vs_trans_socks_sx);
}

static inline void
vs_trans_unlock(void)
{
	sx_xunlock(&vs_trans_socks_sx);
}

static inline void
vs_addr_set(struct sockaddr_vm *addr, uint32_t port)
{
	memset(addr, 0, sizeof(*addr));
	addr->svm_family = AF_VSOCK;
	addr->svm_len = sizeof(*addr);
	addr->svm_port = port;
}

static void
vs_trans_init(void *arg __unused)
{
	VSOCK_DBG(VSOCK_DBG_VERBOSE, "%s: VirtIO Socket vs_trans_init called\n",
	    __func__);

	/* Initialize Globals */
	prev_autobound_port = MAX_PORT;
	sx_init(&vs_trans_socks_sx, "vs_trans_sock_sx");
	mtx_init(&vs_trans_socks_mtx, "vs_trans_socks_mtx", NULL, MTX_DEF);
	LIST_INIT(&vs_trans_bound_socks);
	LIST_INIT(&vs_trans_connected_socks);
}
SYSINIT(vs_trans_init, SI_SUB_PROTO_DOMAIN, SI_ORDER_THIRD, vs_trans_init,
    NULL);

/*
 * Called in two cases:
 * 1) When user calls socket();
 * 2) When we accept new incoming conneciton and call sonewconn().
 */
static int
vs_trans_attach(struct socket *so, int proto, struct thread *td)
{
	struct vs_pcb *pcb = so->so_pcb;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	if (proto != 0 && proto != VIRTIO_SOCK_PROTO_TRANS)
		return (EPROTONOSUPPORT);

	pcb = malloc(sizeof(struct vs_pcb), M_VSOCK, M_NOWAIT | M_ZERO);
	if (pcb == NULL)
		return (ENOMEM);

	pcb->so = so;
	so->so_pcb = pcb;

	return (0);
}

static void
vs_trans_detach(struct socket *so)
{
	struct vs_pcb *pcb;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	vs_trans_lock();
	pcb = so->so_pcb;

	if (SOLISTENING(so)) {
		zfree(pcb, M_VSOCK);
	}

	so->so_pcb = NULL;

	vs_trans_unlock();
}

static int
vs_trans_soreceive(struct socket *so, struct sockaddr **paddr, struct uio *uio,
    struct mbuf **mp0, struct mbuf **controlp, int *flagsp)
{
	// struct vs_pcb *pcb = so->so_pcb;
	int error = 0;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);
	MPASS(uio != NULL && mp0 == NULL);
	return (error);
}

static int
vs_trans_sosend(struct socket *so, struct sockaddr *addr, struct uio *uio,
    struct mbuf *top, struct mbuf *controlp, int flags, struct thread *td)
{
	// struct vs_pcb *pcb = so->so_pcb;
	int error = 0;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);
	MPASS(uio != NULL && top == NULL);
	return (error);
}

static void
vs_trans_abort(struct socket *so)
{
	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	vs_trans_lock();

	if (SOLISTENING(so)) {
		/* Remove from bound list */
		vslist_remove_socket(so, VS_LIST_BOUND, &vs_trans_socks_mtx);
	}

	if (so->so_state & SS_ISCONNECTED) {
		(void)sodisconnect(so);
	}

	vs_trans_unlock();
}

static void
vs_trans_close(struct socket *so)
{
	struct vs_pcb *pcb;
	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	vs_trans_lock();
	pcb = so->so_pcb;

	if (so->so_state & SS_ISCONNECTED) {
		/* Send a FIN to peer */
		VSOCK_DBG(VSOCK_DBG_VERBOSE,
		    "%s: vs_trans_close sending a FIN to host\n", __func__);
		vsock_send_data(pcb->vspcb_tx, NULL, 0, NULL);
	}

	if (so->so_state &
	    (SS_ISCONNECTED | SS_ISCONNECTING | SS_ISDISCONNECTING))
		soisdisconnected(so);

	pcb->so = NULL;

	if (SOLISTENING(so)) {
		/* Remove from bound list */
		vslist_remove_socket(so, VS_LIST_BOUND, &vs_trans_socks_mtx);
	}

	vs_trans_unlock();
}

static int
vs_trans_bind(struct socket *so, struct sockaddr *addr, struct thread *td)
{
	struct vs_pcb *pcb = so->so_pcb;
	struct sockaddr_vm *sa = (struct sockaddr_vm *)addr;
	int error = 0;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	if (sa == NULL) {
		return (EINVAL);
	}

	if (sa->svm_family != AF_VSOCK) {
		VSOCK_DBG(VSOCK_DBG_ERR, "%s: Not supported, sa_family is %u\n",
		    __func__, sa->svm_family);
		return (EAFNOSUPPORT);
	}
	if (sa->svm_len != sizeof(*sa)) {
		VSOCK_DBG(VSOCK_DBG_ERR, "%s: Not supported, sa_len is %u\n",
		    __func__, sa->svm_len);
		return (EINVAL);
	}

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "%s: binding port = 0x%x\n", __func__,
	    sa->svm_port);

	mtx_lock(&vs_trans_socks_mtx);
	if (vslist_find_socket(sa, VS_LIST_BOUND | VS_LIST_CONNECTED, NULL)) {
		error = EADDRINUSE;
	} else {
		/*
		 * The address is available for us to bind.
		 * Add socket to the bound list.
		 */
		vs_addr_set(&pcb->local_addr, sa->svm_port);
		vs_addr_set(&pcb->remote_addr, VMADDR_PORT_ANY);
		vslist_insert_socket(so, VS_LIST_BOUND, NULL);
	}
	mtx_unlock(&vs_trans_socks_mtx);

	return (error);
}

static int
vs_trans_listen(struct socket *so, int backlog, struct thread *td)
{
	struct vs_pcb *pcb = so->so_pcb;
	struct socket *bound_so;
	int error = 0;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	/* Check if the address is already bound and it was by us. */
	bound_so = vslist_find_socket(&pcb->local_addr, VS_LIST_BOUND, NULL);
	if (bound_so == NULL || bound_so != so) {
		VSOCK_DBG(VSOCK_DBG_ERR,
		    "%s: Address not bound or not by us.\n", __func__);
		return (EADDRNOTAVAIL);
	}

	SOCK_LOCK(so);
	error = solisten_proto_check(so);
	if (error == 0)
		solisten_proto(so, backlog);
	SOCK_UNLOCK(so);

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "%s: VirtIO Socket listen error = %d\n",
	    __func__, error);
	return (error);
}

static int
vs_trans_accept(struct socket *so, struct sockaddr *sa)
{
	struct vs_pcb *pcb = so->so_pcb;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	memcpy(sa, &pcb->remote_addr, pcb->remote_addr.svm_len);

	return (0);
}

static int
vs_trans_connect(struct socket *so, struct sockaddr *sa, struct thread *td)
{
	struct vs_pcb *pcb = so->so_pcb;
	struct sockaddr_vm *raddr = (struct sockaddr_vm *)sa;
	bool found_auto_bound_port = false;
	int i, error = 0;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	/* Verify the remote address */
	if (raddr == NULL)
		return (EINVAL);
	if (raddr->svm_family != AF_VSOCK)
		return (EAFNOSUPPORT);
	if (raddr->svm_len != sizeof(*raddr))
		return (EINVAL);

	mtx_lock(&vs_trans_socks_mtx);
	if (so->so_state &
	    (SS_ISCONNECTED | SS_ISDISCONNECTING | SS_ISCONNECTING)) {
		VSOCK_DBG(VSOCK_DBG_ERR,
		    "Virtio Socket %s: socket connect in progress\n", __func__);
		error = EINPROGRESS;
		goto out;
	}

	/*
	 * Find an available port for us to auto bind the local
	 * address.
	 */
	vs_addr_set(&pcb->local_addr, 0);

	for (i = prev_autobound_port - 1; i != prev_autobound_port; i--) {
		if (i == MIN_PORT)
			i = MAX_PORT;

		pcb->local_addr.svm_port = i;

		if (vslist_find_socket(&pcb->local_addr,
			VS_LIST_BOUND | VS_LIST_CONNECTED, NULL) == NULL) {
			found_auto_bound_port = true;
			prev_autobound_port = i;
			VSOCK_DBG(VSOCK_DBG_VERBOSE,
			    "VirtIO Socket %s: found local bound port is %x\n",
			    __func__, pcb->local_addr.svm_port);
			break;
		}
	}

	if (found_auto_bound_port == true) {
		/* Found available port for auto bound, put on list */
		vslist_insert_socket(so, VS_LIST_BOUND, NULL);
		/* Set VM service ID */
		pcb->guest_cid = VMADDR_CID_ANY;
		/* Set host service ID and remote port */
		pcb->host_cid = VMADDR_CID_HOST;
		vs_addr_set(&pcb->remote_addr, raddr->svm_port);

		/* Change the socket state to SS_ISCONNECTING */
		soisconnecting(so);
	} else {
		VSOCK_DBG(VSOCK_DBG_ERR,
		    "VirtIO Socket %s: No local port available for auto bound\n",
		    __func__);
		error = EADDRINUSE;
	}

	VSOCK_DBG(VSOCK_DBG_INFO, "Virtio Socket %s: Connect guest_cid is %lu",
	    __func__, pcb->guest_cid);
	VSOCK_DBG(VSOCK_DBG_INFO, "Connect host_srv_id is %lu\n",
	    pcb->host_cid);

out:
	mtx_unlock(&vs_trans_socks_mtx);

	if (found_auto_bound_port == true) {
		/* XXX connect to virtqueue? */
		return (0);
	}

	return (error);
}

static int
vs_trans_peeraddr(struct socket *so, struct sockaddr *sa)
{
	struct vs_pcb *pcb = so->so_pcb;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	memcpy(sa, &pcb->remote_addr, pcb->remote_addr.svm_len);

	return (0);
}

static int
vs_trans_sockaddr(struct socket *so, struct sockaddr *sa)
{
	struct vs_pcb *pcb = so->so_pcb;

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	memcpy(sa, &pcb->local_addr, pcb->local_addr.svm_len);

	return (0);
}

static int
vs_trans_disconnect(struct socket *so)
{
	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	vs_trans_lock();

	/* If socket is already disconnected, skip this */
	if ((so->so_state & SS_ISDISCONNECTED) == 0)
		soisdisconnecting(so);

	vs_trans_unlock();

	return (0);
}

static int
vs_trans_shutdown(struct socket *so, enum shutdown_how how)
{
	VSOCK_DBG(VSOCK_DBG_VERBOSE, "VirtIO Socket function %s called\n",
	    __func__);

	SOCK_LOCK(so);
	if ((so->so_state &
		(SS_ISCONNECTED | SS_ISCONNECTING | SS_ISDISCONNECTING)) == 0) {
		SOCK_UNLOCK(so);
		return (ENOTCONN);
	}
	SOCK_UNLOCK(so);

	switch (how) {
	case SHUT_RD:
		socantrcvmore(so);
		break;
	case SHUT_RDWR:
		socantrcvmore(so);
		if (so->so_state & SS_ISCONNECTED) {
			/* Send a FIN to peer */
			SOCK_SENDBUF_LOCK(so);
			/* XXX send FIN to peer */
			SOCK_SENDBUF_UNLOCK(so);
			soisdisconnecting(so);
		}
		/* FALLTHROUGH */
	case SHUT_WR:
		socantsendmore(so);
	}
	wakeup(&so->so_timeo);

	return (0);
}

static int
vsock_send_data(struct virtqueue *vq, struct uio *uio, uint32_t to_write,
    struct sockbuf *sb)
{
	struct sglist sg;
	struct sglist_seg segs[2];
	struct virtio_vsock_hdr vs_pkt;
	int error = 0;

	if (vq == NULL) {
		return (ENOTCONN);
	}

	VSOCK_DBG(VSOCK_DBG_VERBOSE, "%s: sending data!\n", __func__);

	/*
	 * Initialize SG
	 * stuff the contents from UIO into a Virtio Header
	 * lock sb
	 * stuff Virtio Header into SG List
	 * unlock sb
	 * put the SG on the vq
	 * notify
	 */

	vs_pkt.len = to_write;

	sglist_init(&sg, 2, segs);
	error = sglist_append(&sg, &vs_pkt, sizeof(vs_pkt));
	KASSERT(error == 0, ("error appending Virtio Header to sglist"));

	if (sb)
		SOCKBUF_UNLOCK(sb);

	error = sglist_append_uio(&sg, uio);
	KASSERT(error == 0, ("error appending UIO data to sglist"));

	if (sb)
		SOCKBUF_LOCK(sb);

	error = virtqueue_enqueue(vq, vq, &sg, 2, 0);
	KASSERT(error == 0, ("error enqueueing NULL buffer to virtqueue"));
	virtqueue_notify(vq);
	return (error);
}

/**
 * Driver glue
 */

struct vsock_softc {
	device_t vsock_dev;
	uint64_t vsock_features;
	struct virtqueue *vsock_rx;
	struct virtqueue *vsock_tx;
	struct virtqueue *vsock_ev;
	struct vs_pcb *vsock_pcb;
	eventhandler_tag eh;
	bool inactive;
};

static int vsock_probe(device_t);
static int vsock_attach(device_t);
static int vsock_detach(device_t);
static int vsock_shutdown(device_t);

static int vsock_negotiate_features(struct vsock_softc *);
static int vsock_setup_features(struct vsock_softc *);
static int vsock_alloc_virtqueue(struct vsock_softc *);

/* VirtIO Socket 1.0 doesn't have any features, but 1.2 will have some */
#define VSOCK_FEATURES 0

static struct virtio_feature_desc vsock_feature_desc[] = { { 0, NULL } };

static device_method_t vsock_methods[] = {
	/* Device methods. */
	DEVMETHOD(device_probe, vsock_probe),
	DEVMETHOD(device_attach, vsock_attach),
	DEVMETHOD(device_detach, vsock_detach),
	DEVMETHOD(device_shutdown, vsock_shutdown),

	DEVMETHOD_END
};

static driver_t vsock_driver = { "vsock", vsock_methods,
	sizeof(struct vsock_softc) };

VIRTIO_DRIVER_MODULE(virtio_socket, vsock_driver, NULL, NULL);
MODULE_VERSION(virtio_socket, 1);
MODULE_DEPEND(virtio_socket, virtio, 1, 1, 1);

VIRTIO_SIMPLE_PNPINFO(virtio_socket, VIRTIO_ID_VSOCK, "VirtIO Socket Adapter");

static int
vsock_probe(device_t dev)
{
	return (VIRTIO_SIMPLE_PROBE(dev, virtio_socket));
}

static int
vsock_attach(device_t dev)
{
	struct vsock_softc *sc;
	int error;

	sc = device_get_softc(dev);
	sc->vsock_dev = dev;
	virtio_set_feature_desc(dev, vsock_feature_desc);

	error = vsock_setup_features(sc);
	if (error) {
		device_printf(dev, "cannot setup features\n");
		goto fail;
	}

	error = vsock_alloc_virtqueue(sc);
	if (error) {
		device_printf(dev, "cannot allocate virtqueues\n");
		goto fail;
	}

	sc->eh = EVENTHANDLER_REGISTER(shutdown_post_sync, vsock_shutdown, dev,
	    SHUTDOWN_PRI_LAST + 1); /* ??? */
	if (sc->eh == NULL) {
		device_printf(dev, "Shutdown event registration failed\n");
		error = ENXIO;
		goto fail;
	}

	sc->inactive = false;

fail:
	if (error)
		vsock_detach(dev);

	return (error);
}

static int
vsock_detach(device_t dev)
{
	struct vsock_softc *sc;

	sc = device_get_softc(dev);

	sc->inactive = true;
	if (sc->eh != NULL) {
		EVENTHANDLER_DEREGISTER(shutdown_post_sync, sc->eh);
		sc->eh = NULL;
	}

	device_printf(dev, "Actually detach the thing here\n");
	return (0);
}

static int
vsock_shutdown(device_t dev)
{
	struct vsock_softc *sc;

	sc = device_get_softc(dev);
	sc->inactive = true;

	return (0);
}

/*
 * Even tho Virtio 1.1 spec defines no Features for Virtio Socket, we'll keep
 * this infrastructure for the future. In 1.2 there will be features.
 */
static int
vsock_negotiate_features(struct vsock_softc *sc)
{
	device_t dev;
	uint64_t features;

	dev = sc->vsock_dev;
	features = VSOCK_FEATURES;

	sc->vsock_features = virtio_negotiate_features(dev, features);
	return (virtio_finalize_features(dev));
}

static int
vsock_setup_features(struct vsock_softc *sc)
{
	int error;

	error = vsock_negotiate_features(sc);
	if (error)
		return (error);

	return (0);
}

static int
vsock_alloc_virtqueue(struct vsock_softc *sc)
{
	device_t dev;
	struct vq_alloc_info vq_info[3];
	int nvqs;

	dev = sc->vsock_dev;
	nvqs = 3;

	VQ_ALLOC_INFO_INIT(&vq_info[0], 0, NULL, sc, &sc->vsock_rx, "%s rx",
	    device_get_nameunit(dev));
	VQ_ALLOC_INFO_INIT(&vq_info[1], 0, NULL, sc, &sc->vsock_rx, "%s tx",
	    device_get_nameunit(dev));
	VQ_ALLOC_INFO_INIT(&vq_info[2], 0, NULL, sc, &sc->vsock_rx, "%s event",
	    device_get_nameunit(dev));

	return (virtio_alloc_virtqueues(dev, nvqs, vq_info));
}
