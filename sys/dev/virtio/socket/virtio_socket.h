/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2023, The FreeBSD Foundation
 * Author: Mina Galić <freebsd@igalic.co>
 */

#ifndef _V_SOCK_H
#define _V_SOCK_H
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/stdint.h>
#include <sys/vm_sockets.h>

/* These Features will come in Virtio 1.2 */
enum {
	/* stream socket type is supported */
	VIRTIO_VSOCK_F_STREAM = 0,
	/* seqpacket socket type is supported */
	VIRTIO_VSOCK_F_SEQPACKET = 1,
};

struct virtio_vsock_hdr {
	uint64_t src_cid;
	uint64_t dst_cid;
	uint32_t src_port;
	uint32_t dst_port;
	uint32_t len;
	uint16_t type;
	uint16_t op; /* see below */
	uint32_t flags;
	uint32_t buf_alloc;
	uint32_t fwd_cnt;
};

enum { VIRTIO_VSOCK_OP_INVALID = 0,

	/*
	 * Connect operations
	 */
	VIRTIO_VSOCK_OP_REQUEST = 1,
	VIRTIO_VSOCK_OP_RESPONSE = 2,
	VIRTIO_VSOCK_OP_RST = 3,
	VIRTIO_VSOCK_OP_SHUTDOWN = 4,

	/* To send payload */
	VIRTIO_VSOCK_OP_RW = 5,

	/* Tell the peer our credit info */
	VIRTIO_VSOCK_OP_CREDIT_UPDATE = 6,
	/* Request the peer to send the credit info to us */
	VIRTIO_VSOCK_OP_CREDIT_REQUEST = 7,
};

struct virtio_vsock_packet {
	struct virtio_vsock_hdr hdr;
	uint8_t data[];
};

struct virtio_vsock_config {
	uint64_t guest_cid;
};

struct vs_pcb {
	struct socket *so; /* Pointer to socket */
	struct sockaddr_vm local_addr;
	struct sockaddr_vm remote_addr;

	uint64_t guest_cid;
	uint64_t host_cid;

	/* Current packet header on rx ring */
	struct virtio_vsock_hdr vs_pkt;
	/* Available data in receive br in current packet */
	uint32_t recv_data_len;
	/* offset in the packet */
	uint32_t recv_data_off;
	struct virtqueue *vspcb_rx;

	/* Available data in transmit br in current packet */
	uint32_t trans_data_len;
	/* offset in the packet */
	uint32_t trans_data_off;
	struct virtqueue *vspcb_tx;

	bool rb_init;
	/* Link lists for global bound and connected sockets */
	LIST_ENTRY(vs_pcb) bound_next;
	LIST_ENTRY(vs_pcb) connected_next;
};

#endif /* _V_SOCK_H */
