/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2023, The FreeBSD Foundation
 * Author: Mina Galić <freebsd@igalic.co>
 */

#ifndef _VM_SOCKETS_H
#define _VM_SOCKETS_H

/*
 * VirtIO Socket Protocols
 */
enum {
	/* Transport protocol */
	VIRTIO_SOCK_PROTO_TRANS = 1,

	VMADDR_PORT_ANY = -1U,
	VMADDR_CID_ANY = -1U,
	VMADDR_CID_HYPERVISOR = 0,
	VMADDR_CID_LOCAL = 1,
	VMADDR_CID_HOST = 2,
	/* Linux compatibility */

	/* Option name for STREAM socket buffer size.  Use as the option name in
	 * setsockopt(3) or getsockopt(3) to set or get an unsigned long long
	 * that specifies the size of the buffer underlying a vSockets STREAM
	 * socket. Value is clamped to the MIN and MAX.
	 */
	SO_VM_SOCKETS_BUFFER_SIZE = 0,

	/* Option name for STREAM socket minimum buffer size.  Use as the option
	 * name in setsockopt(3) or getsockopt(3) to set or get an unsigned long
	 * long that specifies the minimum size allowed for the buffer
	 * underlying a vSockets STREAM socket.
	 */
	SO_VM_SOCKETS_BUFFER_MIN_SIZE = 1,

	/* Option name for STREAM socket maximum buffer size.  Use as the option
	 * name in setsockopt(3) or getsockopt(3) to set or get an unsigned long
	 * long that specifies the maximum size allowed for the buffer
	 * underlying a vSockets STREAM socket.
	 */
	SO_VM_SOCKETS_BUFFER_MAX_SIZE = 2,

	/* Option name for socket peer's host-specific VM ID.  Use as the option
	 * name in getsockopt(3) to get a host-specific identifier for the peer
	 * endpoint's VM.  The identifier is a signed integer. Only available
	 * for hypervisor endpoints.
	 */
	SO_VM_SOCKETS_PEER_HOST_VM_ID = 3,

	/* Option name for determining if a socket is trusted.  Use as the
	 * option name in getsockopt(3) to determine if a socket is trusted. The
	 * value is a signed integer.
	 */
	SO_VM_SOCKETS_TRUSTED = 5,

	/* Option name for STREAM socket connection timeout.  Use as the option
	 * name in setsockopt(3) or getsockopt(3) to set or get the connection
	 * timeout for a STREAM socket.
	 */
	SO_VM_SOCKETS_CONNECT_TIMEOUT_OLD = 6,

	/* Option name for using non-blocking send/receive.  Use as the option
	 * name for setsockopt(3) or getsockopt(3) to set or get the
	 * non-blocking transmit/receive flag for a STREAM socket.  This flag
	 * determines whether send() and recv() can be called in non-blocking
	 * contexts for the given socket.  The value is a signed integer.
	 *
	 * This option is only relevant to kernel endpoints, where descheduling
	 * the thread of execution is not allowed, for example, while holding a
	 * spinlock. It is not to be confused with conventional non-blocking
	 * socket operations.
	 *
	 * Only available for hypervisor endpoints.
	 */
	SO_VM_SOCKETS_NONBLOCK_TXRX = 7,

	SO_VM_SOCKETS_CONNECT_TIMEOUT_NEW = 8,
};

struct sockaddr_vm {
	uint8_t svm_len;
	sa_family_t svm_family;
	uint16_t _svm_reserved1;
	uint32_t svm_port;
	uint64_t svm_cid;
	uint8_t svm_flags;
};

#endif /* _VM_SOCKETS_H */
